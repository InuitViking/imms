<?php

declare(strict_types=1);

namespace Imms\Interfaces;

use League\Plates\Engine;

/**
 * A theme interface to outline the necessary features of an IMMS theme
 *
 * @property-read Engine $template
 */
interface ThemeInterface {
    public function __construct();

    public function setEngine(): void;
    public function getEngine(): Engine;
}