<?php

declare(strict_types=1);

namespace Imms\Extensions\LastTag;

use League\CommonMark\Extension\ExtensionInterface;
use League\CommonMark\Environment\EnvironmentBuilderInterface;

final class LastTagExtension implements ExtensionInterface{

    /**
     * Simply registers the extension and the included parser
     *
     * @param EnvironmentBuilderInterface $environment
     * @return void
     */
    public function register(EnvironmentBuilderInterface $environment): void {
        $environment->addInlineParser(new LastTagParser, priority: 10);
    }
}