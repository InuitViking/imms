<?php

declare(strict_types=1);

namespace Imms\Extensions\SpacedLinksProcessor;

use Gitlab\Client;
use Imms\Classes\Bootstrapper;
use Imms\Gitlab\Gitlab;
use League\CommonMark\Event\DocumentParsedEvent;
use League\CommonMark\Extension\CommonMark\Node\Inline\Link;
use League\CommonMark\Node\Inline\Text;
use League\Flysystem\Filesystem;
use League\Flysystem\FilesystemException;
use League\Flysystem\Local\LocalFilesystemAdapter;
use Phalcon\Assets\Inline;

// https://github.com/thephpleague/commonmark/discussions/1028

class SpacedLinksProcessor {
    public function onDocumentParsed(DocumentParsedEvent $event): void {
        $document = $event->getDocument();
        $walker = $document->walker();
        $previousLiteral = '';
        while ($event = $walker->next()) {
            $node = $event->getNode();

            // If it is a Link block and we have a previous literal
            if ($previousLiteral !== '' && $node instanceof Link) {
                $this->fixExternalSpacedLink($node, $previousLiteral);
                $previousLiteral = '';
            }

            // If it's not a Text block, continue.
            if (!($node instanceof Text) || !$event->isEntering()) {
                continue;
            }

            // If it looks like the start of link markdown, but some of it is missing, save this prefix for later.
            if (preg_match('/\[.*]\($/i', $node->getLiteral())) {
                $previousLiteral = $node->getLiteral();
                $node->detach();
            }

            // If it's seemingly just an internal link, then create it as a simple link
            if ($this->isLiteralSpacedLink($node->getLiteral())) {
                $node->replaceWith($this->createLinkFromSpacedLink($node));
            }
        }
    }

    private function fixExternalSpacedLink ($node, $previousLiteral): void {
        // Remove the markdown.
        $literal = str_replace(['[', ']('], '', $previousLiteral);
        // Get the next node, which is very likely to be the rest of the link and markdown
        $nextNode = $node->next();

        // If it is an instance of Text
        if ($nextNode instanceof Text) {
            // Remove it
            $nextNode->detach();
            // Get its contents and remove the markdown
            $nextLiteral = str_replace(' ', '%20', $nextNode->getLiteral());
            $nextLiteral = str_replace(')', '', $nextLiteral);

            // Replace the node with a new Link node, and reset the previous literal
            $node->replaceWith(new Link($node->getUrl() . $nextLiteral, $literal, $literal));
        }
    }

    /**
     * Checks whether a literal (string) is a version string.
     *
     * @param string $literal
     * @return bool
     */
    private function isLiteralSpacedLink(string $literal): bool {
        // Only look at http and https URLs
        if (!preg_match("/\[.*]\(.*\)/i", $literal)) {
            return false;
        }

        return true;
    }

    /**
     * Create link from a passed Text node.
     *
     * @param Text $node
     * @return Link
     */
    private function createLinkFromSpacedLink (Text $node): Link {
        $literal = explode(']', $node->getLiteral());
        $literal = str_replace('[', '', $literal[0]);
        $link = explode('(', $node->getLiteral());
        $link = str_replace(')', '', $link[1]);
        $link = str_replace(' ', '-', $link);
        return new Link($link, $literal, $literal);
    }
}