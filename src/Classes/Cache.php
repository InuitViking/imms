<?php

declare(strict_types=1);

namespace Imms\Classes;
require_once '../vendor/autoload.php';

use ElGigi\CommonMarkEmoji\EmojiExtension;
use Imms\Extensions\LastTag\LastTagExtension;
use Imms\Extensions\SpacedLinks\SpacedLinksExtension;
use Imms\Extensions\SpacedLinksProcessor\SpacedLinksProcessor;
use Jnjxp\CommonMarkWikiLinks\WikiLinkExtension;
use League\CommonMark\Environment\Environment;
use League\CommonMark\Event\DocumentParsedEvent;
use League\CommonMark\Exception\CommonMarkException;
use League\CommonMark\Extension\Attributes\AttributesExtension;
use League\CommonMark\Extension\CommonMark\CommonMarkCoreExtension;
use League\CommonMark\Extension\ExternalLink\ExternalLinkExtension;
use League\CommonMark\Extension\Footnote\FootnoteExtension;
use League\CommonMark\Extension\GithubFlavoredMarkdownExtension;
use League\CommonMark\Extension\HeadingPermalink\HeadingPermalinkExtension;
use League\CommonMark\Extension\TableOfContents\TableOfContentsExtension;
use League\CommonMark\MarkdownConverter;
use League\Flysystem\Filesystem;
use League\Flysystem\FilesystemException;
use League\Flysystem\Local\LocalFilesystemAdapter;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use N0sz\CommonMark\Marker\MarkerExtension;
use ScssPhp\ScssPhp\Exception\SassException;
use SimonVomEyser\CommonMarkExtension\LazyImageExtension;
use Sven\CommonMark\ImageMediaQueries\ImageMediaQueriesExtension;
use Ueberdosis\CommonMark\HintExtension;
use Zoon\CommonMark\Ext\YouTubeIframe\YouTubeIframeExtension;

class Cache {

    /**
     * Property for storing the IMMS config
     *
     * @var array|false $config
     */
    private array|false $config;

    /**
     * Property for storing the config for Commonmarks config
     *
     * @var array $mdConfig
     */
    private array $mdConfig;

    /**
     * The environment for Commonmark
     *
     * @var Environment $environment
     */
    private Environment $environment;

    /**
     * A filesystem object from Flysystem
     *
     * @var Filesystem $filesystem
     */
    private Filesystem $filesystem;

    /**
     * The adapter we're going to use for IMMS.
     * There are no plans for using other filesystems (demand my change that)
     *
     * @var LocalFilesystemAdapter $adapter
     */
    private LocalFilesystemAdapter $adapter;

    /**
     * Private Theme for theme operations.
     *
     * @var Theme
     */
    private Theme $theme;

    /**
     * The path to all checksums
     *
     * @var string $checksumPath
     */
    private string $checksumPath;

    /**
     * This solely exists to make sonarlint shut up.
     *
     * I'm a professional, trust me.
     *
     * @var string $htmlExt
     */
    private string $htmlExt = '.html';
    private MarkdownConverter $converter;
    private Logger $log;

    /**
     * The Cache class takes care of rendering markdown into HTML.
     * This is mostly used to pre-render the markdown and store it to a cache directory for faster load times.
     * There are also methods for clearing caches.
     */
    public function __construct (Theme $theme) {
        $this->config = Bootstrapper::getIni();
        $this->checksumPath = Bootstrapper::rootDirectory() . '/src/checksums/';

        $this->adapter = new LocalFilesystemAdapter(Bootstrapper::rootDirectory());
        $this->filesystem = new Filesystem(
            $this->adapter,
            ['checksum_algo' => 'sha256']
        );
        $this->theme = $theme;

        $this->log = new Logger('Cache');
        $this->log->pushHandler(new StreamHandler($this->config['app']['log_path'], Logger::WARNING));

        $this->mdConfig = $this->setupMDConverterConfig();
        $this->environment = $this->prepareConverterEnvironment($this->mdConfig);
        $this->converter = new MarkdownConverter($this->environment);
    }

    /**
     * Renders a single markdown file into HTML.
     *
     * @param string $file
     * @param array|null $settings
     *
     * @return void
     *
     * @throws CommonMarkException
     * @throws SassException
     * @throws FilesystemException
     */
    public function md2html (string $file, ?array $settings = null): void {

        // Create the folder structure for the cache and checksums (default)
        $cacheFolderStructure = $this->createChecksumDirs($file, $settings);
        $rootDir = Bootstrapper::rootDirectory();

        // Use these settings instead, if we're running from a CLI interface
        if (isset($settings['cli']) && $settings['cli'] === true) {
            $settings['no_scss'] = true;
            $settings['error_page'] = false;
            $cacheFolderStructure = $settings['output_dir'] ?? '';
        }

        // If git is enabled, fetch and pull changes before we cache stuff.
        if (GitDocuments::gitEnabled()) {
            $git = new GitDocuments();
            $git->fetchPull();
        }

        // If file doesn't exist, just return
        if (!$this->filesystem->fileExists($file)) {
            $this->log->warning("File '$file' doesn't exist."); return;
        }

        $basenameFile = basename($file);
        // Generate path to HTML file
        $htmlFile = str_replace('.md', $this->htmlExt, $basenameFile);
        $htmlFile = $cacheFolderStructure . $htmlFile;
        $fileContents = $this->filesystem->read($file);

        if ($settings === null) {
            // Compile SCSS
            $this->theme->compileSCSS();

            // Generate path to checksum file
            $checksumFile = str_replace($this->config['app']['md_path'], '', $file);
            // Get the contents of the file and create a checksum of it
            $checksum = $this->filesystem->checksum($file);
            $checksumPath = 'src/checksums/';
            $checksumFile = $checksumPath . $checksumFile;
            $this->filesystem->write($checksumFile, $checksum);
        }

        // Create HTML - make sure uploads are done properly
        $templateEngine = $this->theme->themeObject->getEngine();
        $html = $templateEngine->render('partials/header');
        $html .= $this->converter->convert($fileContents)->getContent();
        $html .= $templateEngine->render('partials/footer');
        $html = str_replace('uploads', '/uploads', $html);

        // Figure out the path to the HTML file
        $path = $this->config['app']['html_path'] ?? 'web/assets/cache/';
        if ($settings !== null && $settings['error_page'] === true) {
            $htmlFile = str_replace($this->config['app']['error_md_path'], '', $htmlFile);
            $path = $this->config['app']['error_html_path'] ?? 'web/assets/cache/errors/';
        }

        $htmlFile = $path . $htmlFile;
        if (isset($settings['cli']) && $settings['cli']) {
            $htmlFile = $rootDir . $htmlFile;
        }

        // Put $html into $htmlFile
        $this->filesystem->write($htmlFile, $html);
    }

    /**
     * Renders a string of markdown into HTML; returns the resulting markdown as a string
     *
     * @throws CommonMarkException
     */
    public function md2HtmlNoFile (string $md): string {
        return $this->converter->convert($md)->getContent();
    }

    /**
     * Clears the HTML cache for a single URL path (usually just a single HTML file).
     *
     * Example:
     *
     * The url is example.com/php/functions/file_exists, the "urlPath" will then be "php/functions/file_exists".
     *
     * @param $urlPath
     * @param bool $cli
     *
     * @return void
     */
    public function clearCacheSingularURL ($urlPath, bool $cli = false): void {

        $path = $this->config['app']['html_path'] ?? 'web/assets/cache/';

        if ($cli && file_exists(Bootstrapper::rootDirectory() . $path . $urlPath . $this->htmlExt)) {
            unlink(Bootstrapper::rootDirectory() . $path . $urlPath . $this->htmlExt);
        }

        if (file_exists(Bootstrapper::rootDirectory() . $path . $urlPath . $this->htmlExt)) {
            unlink(Bootstrapper::rootDirectory() . $path . $urlPath . $this->htmlExt);
        }
    }

    /**
     * Clears the entire cache. Markdown will be rendered on first load.
     *
     * @param $cachePath
     *
     * @return void
     */
    public function clearCache ($cachePath): void {
        $rootDir = Bootstrapper::rootDirectory();
        $files = glob($rootDir.'/'.$cachePath . '*');

        try {
            foreach ($files as $file) {
                $fileToDelete = str_replace("$rootDir/", '', $file);
                    if (is_file($file)) {
                        $this->filesystem->delete($fileToDelete);
                    }
                    if (is_dir($file) && $fileToDelete !== $this->config['app']['html_path'] . 'errors') {
                        $this->filesystem->deleteDirectory($fileToDelete);
                    }
            }
        } catch (FilesystemException $e) {
            $this->log->error($e->getMessage());
            $this->log->error($e->getTraceAsString());
        }
    }

    /**
     * Creates the necessary directories (also nested), to properly serve the cache.
     *
     * @param string $filepath
     * @param array|null $settings
     *
     * @return string
     */
    private function createChecksumDirs (string $filepath, ?array $settings = null): string {
        $filepath = str_replace($this->config['app']['md_path'], '', $filepath);
        // Fetch folder structure
        $folderStructure = $this->getFolderStructure($filepath, $settings);

        // Create the folders if they don't exist
        if (strlen($folderStructure) > 0) {
            if (!is_dir($this->checksumPath . $folderStructure)) {
                mkdir($this->checksumPath . $folderStructure, 0775, true);
            }
            $folderStructure .= '/';
        }

        return $folderStructure;
    }

    /**
     * Gets the folder structure of the file
     *
     * @param string $filepath
     * @param array|null $settings
     *
     * @return string
     */
    private function getFolderStructure(string $filepath, ?array $settings = null): string {
        if ($settings !== null && isset($settings['error_page']) && $settings['error_page'] === true) {
            $path = $this->config['app']['error_md_path'] ?? 'src/errors/';
        } else {
            $path = $this->config['app']['md_path'] ?? 'src/documents/';
        }
        $folderStructure = str_replace(Bootstrapper::rootDirectory() . $path, '', $filepath);
        $length = strrpos($folderStructure, '/');
        if (!$length) {
            $length = 0;
        }
        return substr($folderStructure, 0, $length);
    }

    /**
     * Returns the configuration for the commonmark converter.
     *
     * @return array
     */
    private function setupMDConverterConfig (): array {
        return [
            'heading_permalink' => [
                'html_class' => 'heading-permalink',
                'id_prefix' => '',
                'fragment_prefix' => '',
                'insert' => 'after',
                'min_heading_level' => $this->config['heading_permalinks']['min_heading_level'] ?? 1,
                'max_heading_level' => $this->config['heading_permalinks']['max_heading_level'] ?? 6,
                'title' => $this->config['heading_permalinks']['link_title'] ?? 'Link to header',
                'symbol' => $this->config['heading_permalinks']['link_symbol'] ?? '🔗',
                'aria_hidden' => false
            ],
            'external_link' => [
                'internal_hosts' => $this->config['external_links']['internal_hosts'] ?? 'localhost',
                'open_in_new_window' => $this->config['external_links']['open_in_new_window'] ?? true,
                'html_class' => 'external-link',
                'nofollow' => '',
                'noopener' => 'external',
                'noreferrer' => 'external',
            ],
            'footnote' => [
                'backref_class' => 'footnote-backref',
                'backref_symbol' => $this->config['footnotes']['backref_symbol'] ?? '↩',
                'container_add_hr' => $this->config['footnotes']['add_hr'] ?? true,
                'container_class' => 'footnotes',
                'ref_class' => 'footnote-ref',
                'ref_id_prefix' => 'fnref:',
                'footnote_class' => 'footnote',
                'footnote_id_prefix' => 'fn:',
            ],
            'youtube_iframe' => [
                'width' => (string)$this->config['youtube_iframe']['width'] ?? 600,
                'height' => (string)$this->config['youtube_iframe']['height'] ?? 300,
                'allow_full_screen' => $this->config['youtube_iframe']['allow_fullscreen'] ?? true,
            ],
            'table_of_contents' => [
                'html_class' => 'table-of-contents',
                'style' => $this->config['toc']['style'],
                'min_heading_level' => $this->config['toc']['min_heading_level'] ?? 1,
                'max_heading_level' => $this->config['toc']['max_heading_level'] ?? 6,
                'normalize' => 'relative',
                'position' => 'placeholder',
                'placeholder' => $this->config['toc']['placeholder'] ?? '[[_TOC_]]',
            ],
            'allow_unsafe_links' => false
        ];
    }

    /**
     * Returns the configured commonmark environment and bases it off of the supplied config.
     *
     * @param array $config
     *
     * @return Environment
     */
    private function prepareConverterEnvironment (array $config): Environment {
        $environment = new Environment($config);

        // If a plugin wants to replace the latest tag, instead of using the default one.
        $eventGetExtensions = PluginsMediator::staticNotifyCommonMarkExtensions('replace_latest_tag');
        if (!empty($eventGetExtensions)) {
            foreach ($eventGetExtensions as $eventGetExtension) {
                $environment->addExtension($eventGetExtension);
            }
        } else {
            $environment->addExtension(new LastTagExtension());
        }

        // Add default extensions
        $environment->addExtension(new CommonMarkCoreExtension());
        $environment->addExtension(new HeadingPermalinkExtension());
        $environment->addExtension(new TableOfContentsExtension());
        $environment->addExtension(new GithubFlavoredMarkdownExtension());
        $environment->addExtension(new AttributesExtension());
        $environment->addExtension(new HintExtension());
        $environment->addExtension(new ExternalLinkExtension());
        $environment->addExtension(new FootnoteExtension());
        $environment->addExtension(new YouTubeIframeExtension());
        $environment->addExtension(new EmojiExtension());
        $environment->addExtension(new MarkerExtension());
        $environment->addExtension(new ImageMediaQueriesExtension());
        $environment->addExtension(new WikiLinkExtension());
        $environment->addExtension(new LazyImageExtension());

        // Add custom extensions from plugins
        unset($eventGetExtensions);
        $eventGetExtensions = PluginsMediator::staticNotifyCommonMarkExtensions('add_commonmark_extension');
        if (!empty($eventGetExtensions)) {
            foreach ($eventGetExtensions as $eventGetExtension) {
                $environment->addExtension($eventGetExtension);
            }
        }

        // Add default DocumentParsedEvent Listeners
        $environment->addEventListener(DocumentParsedEvent::class, [new SpacedLinksProcessor(), 'onDocumentParsed']);

        // Add custom DocumentParsedEvent Listeners
        unset($eventGetExtensions);
        $eventGetExtensions = PluginsMediator::staticNotifyCommonMarkEventListeners('add_commonmark_event_listener');
        if (!empty($eventGetExtensions)) {
            foreach ($eventGetExtensions as $eventGetExtension) {
                $listener = $eventGetExtension;
                $environment->addEventListener(DocumentParsedEvent::class, [$listener, 'onDocumentParsed']);
            }
        }

        return $environment;
    }

}
