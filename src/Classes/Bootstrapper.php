<?php

declare(strict_types=1);

namespace Imms\Classes;

class Bootstrapper {

    private static array|false $ini;

    /**
     * Gets the configuration from the Ini file; returns an array of the entire configuration.
     *
     * @return array|false
     */
    public static function getIni (): array|false {
        return self::$ini;
    }

    /**
     * Set/Change the ini configuration file you want to get the configuration from.
     *
     * @param string|null $iniPath
     *
     * @return void
     */
    public static function setIni (string $iniPath = null): void {
        if ($iniPath === null) {
            self::$ini = parse_ini_file(Bootstrapper::rootDirectory() . '/config/config.ini', true, INI_SCANNER_TYPED);
        } else {
            self::$ini = parse_ini_file($iniPath, true, INI_SCANNER_TYPED);
        }

    }

    /**
     * Static function to return the root directory of the project.
     *
     * @return string
     */
    public static function rootDirectory (): string {
        // Change the second parameter to suit your needs
        return dirname(__FILE__, 3);
    }
}
