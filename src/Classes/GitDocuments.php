<?php

declare(strict_types=1);

namespace Imms\Classes;

use DateTime;
use Exception;
use CzProject\GitPhp\Git;
use League\Flysystem\Filesystem;
use CzProject\GitPhp\GitRepository;
use League\Flysystem\FilesystemException;
use League\Flysystem\Local\LocalFilesystemAdapter;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class GitDocuments {

    /**
     * String containing the address to the remote git repository
     *
     * @var string
     */
    private string $remote;

    /**
     * The string containing the branch name to pull from
     *
     * @var string
     */
    private string $branch;

    /**
     * The path to the repository. This is the md_path.
     *
     * @var string
     */
    private string $path;

    /**
     * The configuration from the standard IMMS configuration
     *
     * @var array|
     */
    private array $ini;

    /**
     * Used for cloning / opening a repository locally
     *
     * @var Git
     */
    private Git $git;

    /**
     * Used for fetching, pulling, and other typical git commands
     *
     * @var false|GitRepository
     */
    private GitRepository|false $gitRepository;


    /**
     * Used for logging.
     *
     * @var Logger
     */
    private Logger $log;

    /**
     * GitDocuments is an object containing methods to clone, fetch, and pull changes from a remote git repository.
     */
    public function __construct () {
        $this->ini = Bootstrapper::getIni();
        $this->remote = $this->ini['git']['remote'];
        $this->branch = $this->ini['git']['branch'] ?? '';
        $this->path = Bootstrapper::rootDirectory() . $this->ini['app']['md_path'];
        $this->git = new Git();

        $this->log = new Logger('GitDocuments');
        $this->log->pushHandler(new StreamHandler($this->ini['app']['log_path'], Logger::WARNING));

        if ($this->isCloned()) {
            $this->gitRepository = $this->git->open($this->path);
        } else {
            $this->gitRepository = $this->clone();
        }
    }

    /**
     * Checks whether the git feature is enabled.
     *
     * @return bool
     */
    public static function gitEnabled (): bool {
        return Bootstrapper::getIni()['git']['enabled'] ?? false;
    }

    /**
     * Clones the specified remote repository from the specified branch, to the md_path
     *
     * @return GitRepository|false
     */
    public function clone (): false|GitRepository{

        // Check if repo is already cloned
        if ($this->isCloned()) {
            return false;
        }

        // Check if the remote url is readable
        if (!$this->git->isRemoteUrlReadable($this->remote)) {
            die ("ERROR: The repository ($this->remote) and/or the branch ($this->branch) does not exist.");
        }

        try {
            // Get the repository and store it in a variable
            $repository = $this->git->cloneRepository($this->remote, $this->path);

            // Checkout to a specific branch if specified
            if ($this->branch !== '') {
                $repository->checkout($this->branch);
            }

            // Return the repository
            return $repository;

        } catch (Exception $e) {
            $this->log->error($e->getMessage());
            $this->log->error($e->getTraceAsString());
            return false;
        }
    }

    /**
     * Checks whether a .git directory is already present at the document path
     *
     * Returns true if yes, and false if not.
     *
     * @return bool
     */
    public function isCloned (): bool {
        if (is_dir($this->path.'/.git')) {
            return true;
        }
        return false;
    }

    /**
     * Fetches and pulls changes.
     * Only fetches and pulls if 5 minutes have passed; this is so we don't spam the remote end point.
     *
     * @return void
     */
    public function fetchPull(): void {
        try {
            $adapter = new LocalFilesystemAdapter('/tmp');
            $filesystem = new Filesystem($adapter);

            $now = new DateTime('now');

            if (!$filesystem->fileExists('imms-last-fetch')) {
                $filesystem->write('imms-last-fetch', $now->format('d-m-Y-H-i-s'));
            }

            if ($filesystem->fileExists('imms-last-fetch')
                && ($now->getTimestamp() - $filesystem->lastModified('imms-last-fetch')) >= 300) {
                $this->gitRepository->fetch();
                $this->gitRepository->pull();

                $filesystem->write('imms-last-fetch', $now->format('d-m-Y-H-i-s'));
            }
        } catch (Exception | FilesystemException $e) {
            $this->log->error($e->getMessage());
            $this->log->error($e->getTraceAsString());
        }
    }
}
