<?php

declare(strict_types=1);

require_once '../vendor/autoload.php';

use Imms\Classes\Cache;
use Imms\Classes\PluginsMediator;
use Imms\Classes\Route;
use Imms\Classes\Bootstrapper;
use Imms\Classes\Theme;
use League\Container\Container;
use League\Container\ReflectionContainer;

// Setup some required variables
Bootstrapper::setIni();
$config = Bootstrapper::getIni();

$container = new Container();

$container->delegate(new ReflectionContainer(true));

$plugins = new PluginsMediator($container);

// Notify plugins of a page load
$plugins->notifyToEvent('page_load');

// Add Theme and instantiate it
$theme = $container->get(Theme::class);

// Add Cache and instantiate.
$container
    ->add(Cache::class)
    ->addArgument($theme);
$cache = $container->get(Cache::class);
// Add Route
$container
    ->add(Route::class)
    ->addArgument($cache)
    ->addArgument($theme)
    ->addArgument($plugins);

// Instantiate the route class and display the current page
$route = $container->get(Route::class);
$route->displayPage();

// Notify plugins that page load has ended.
$plugins->notifyToEvent('page_end');
